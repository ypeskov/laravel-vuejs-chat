<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>Chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo e(asset('css/app.css')); ?>" />

    <style>
        .list-group {
            overflow-y: scroll;
            height: 200px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div id="app" class="row">
            <div class="offset-4 col-4 offset-sm-1 col-sm-10">
                <li class="list-group-item active">Chat Room
                    <span class="badge badge-warning badge-pill">{{ numberOfUsers }}</span>
                </li>

                <div class="badge badge-pill badge-primary">{{ typing }}</div>

                <ul class="list-group" v-chat-scroll>
                    <message
                        v-for="value, index in chat.message"
                        :key='value.index'
                        :user='chat.user[index]'
                        :color='chat.color[index]'
                        :time='chat.time[index]'
                    >
                        {{ value }}
                    </message>


                </ul>                    
                <input type="text" 
                    class="form-control" 
                    placeholder="Enter message..." 
                    @keyup.enter='send'
                    v-model='message' />
            </div>
        </div>
    </div>
    
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
</body>
</html>